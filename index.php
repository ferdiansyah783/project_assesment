<?php

session_start();

if (!isset($_SESSION['login'])){
    header("location: registrasi/login.php");
}

require 'registrasi/register.php';

require 'functions.php';
$pokemon=comand('SELECT * from pokemon ORDER BY id DESC', PDO::FETCH_ASSOC);

if (isset($_POST['search'])){
    $pokemon= searching($_POST['keyword']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="images/icons.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="test.css">
    <title>Pokemon MF</title>

</head>
<body>

    <div class="container mt-3">
        <div class="row">
            
            <div class="col-12">
                <center class="mb-3"><img src="images/logo.png" width="170" alt="pokemon"></center>
                <h1 class="text-center rounded text-primary border border-primary p-2">Pokemon<span class="text-danger">.com</span></h1>
                <label for="keyword" id="pokemon-search" class="text-light">Search by name,elemen or addtional elements</label>
                <form action="" method="POST" class="form-inline my-2 my-lg-0">
                    <input class="form-control col-11 mr-sm-2 bg-transparent text-info border-info" type="search"  placeholder="Search" aria-label="Search" autocomplete="off" autofocus name="keyword" id="keyword">
                    <button class="btn btn-outline-info text-info bg-transparent" type="submit" name="search" id="keyword">Search</button>
                </form>

                <div class="mt-3">
                    <div id="flip" class="bg-transparent text-center text-light">See more about pokemon <br>
                        <svg id="icon" width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                        </svg>
                    </div>
                    <div id="panel" class="text-light bg-transparent text-center rounded-top p-1">
                        Pokémon are mysterious creatures with many secrets. Some Pokémon live with humans and some live in the wild in grasslands, caves, or the sea, but much about their ecology is still unknown. One of their main features is that they can be caught using Poké Balls, which allows them to be carried around. <br><br>
                        Pokémon adalah makhluk misterius dengan banyak rahasia. Beberapa Pokémon tinggal bersama manusia dan beberapa tinggal di alam bebas di padang rumput, gua, atau laut, tapi banyak hal tentang ekologi mereka yang masih belum diketahui. Salah satu fitur utama mereka adalah bahwa mereka dapat ditangkap menggunakan Poké Ball, yang memungkinkan mereka dibawa-bawa.
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div>
                    <?php $i=1; ?>
                    <?php foreach($pokemon as $row) : ?>
                    <div id="card" class="card bg-transparent">
                        <img src="images/<?=$row['gambar']; ?>" class="card-img-top" alt="...">
                        <div class="card-body border border-light rounded-top">
                            <h5 class="card-title text-light"><?=$i; ?>. <?=$row['nama']; ?></h5>
                            <div style="list-style: none;">
                                <div id="pokemon_type2" class="size-14">
                                    <span style="size: 100%"><?=$row['elemen']; ?></span>
                                </div>
                                <div id="pokemon_type" class="size-14">
                                    <span style="size: 100%"><?=$row['skill']; ?></span>
                                </div>
                                <div id="setting">
                                    <a id="delete" href="delete.php?id=<?=$row['id']; ?>"><span type="button" class="btn bg-transparent text-warning" onclick="return confirm('are you sure wont to delete this?');">delete</span></a> | 
                                    <a href="update.php?id=<?=$row['id']; ?>"><span type="button" class="btn bg-transparent text-warning">Update</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            
            
        </div>
    </div>
    
    
    <div class="container-fluid">
        <div>
            <a href="insert.php"><h2 id="link" class="text-center border border-primary text-primary mt-3">Create New Pokemon</h2></a>
        </div>
        <div>
            <a href="registrasi/logout.php"><h2 id="link" style="width: 30%; margin-left: 14.5em;" class="text-center border border-primary text-primary mt-1">Log Out</h2></a>
        </div>

        <div>
            <div class="footer-icon" style="padding-top: 100px;">
                <svg width="2.2em" height="2.2em" viewBox="0 0 16 16" class="bi bi-chat-dots text-light mb-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                    <path d="M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                </svg> | 
                <a href="https://wa.me/6287894573986">
                    <i class="fab fa-whatsapp text-light" style="font-size: 35px;"></i>
                </a> | 
                <a href="https://github.com/ferdiansyah783">
                    <i style="font-size: 30px;" class="fab fa-github text-light"></i>
                </a>
            </div>
            
            <div class="footer-nav">
                <ul>
                    <li><a href="#">Apa itu pokemon</a></li>
                    <li> | </li>
                    <li><a href="#">Syarat pengguanan</a></li>
                    <li> | </li>
                    <li><a href="#">Peta situs</a></li>
                </ul>
                <br>
                <div class="footer-content">
                    <small>
                        ©2020 Pokémon. ©2020 Muhamad ferdiansyah/Creatures/project asassement <br>
                        ©2020 muhammad ferdinasyah, Creatures, project asassement, Tangerang, Cipondoh, Blok M-59. ©Pokémon. TM and ® are trademarks of Muhammad ferdiansyah. 
                    </small>
                </div>
            </div>
        </div>
    </div>


    <!-- optional js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <!-- jquery -->
    <script> 
        $(document).ready(function(){
            $("#flip").click(function(){
                $("#panel").slideToggle("slow");
            });
        });

    </script>

</body>
</html>