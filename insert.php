<?php

require 'functions.php';

// cek tombol submit
if (isset($_POST['submit'])){

    // cek apakah data berhasil ditambahkan
    if (insert($_POST) > 0){
        echo "<script>
                alert('data berhasil ditambahkan');
                document.location.href='index.php';
            </script>
        ";
    }else{
        echo "<script>
                alert('data gagal ditambahkan');
                document.location.href='index.php';
            </script>
        ";
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>insert</title>
    <style>
        body{
            background-image: url(images/background2.png);
            background-size: cover;
        }
    </style>
</head>
<body>
    
    <!-- insert -->
    <div class="container mt-5">
        <h2 class="text-primary text-center border border-primary rounded p-2">Insert New Pokemon</h2>
        <div class="row">
            <div class="col">

                <form action="" autocomplete="off" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="text-light">Name</label>
                        <input type="text" maxlength="15" name="nama" autocomplete="off" placeholder="input name" class="form-control bg-transparent text-info border-info" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="elemen" class="text-light">Type</label>
                        <select class="custom-select text-info bg-transparent border-info" name="elemen" id="validationCustom04" required autofocus>
                            <option selected disabled value="">Choose Type</option>
                            <option value="Fire">Fire</option>
                            <option value="Water">Water</option>
                            <option value="Soil">Soil</option>
                            <option value="Lightning">Lightning</option>
                            <option value="Wind">Wind</option>
                            <option value="Fairy">Fairy</option>
                            <option value="Grass">Grass</option>
                            <option value="Poison">Poison</option>
                            <option value="Poison">Es</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="text-light">Additional Elementse</label>
                        <input type="text" name="skill" maxlength="10" autocomplete="off" placeholder="input additional elements" class="form-control bg-transparent text-info border-info" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="elemen" class="text-light">Choose Image</label>
                        <input type="file" name="gambar" autocomplete="off" placeholder="Choose Image" class="bg-transparent text-info border-info" required autofocus>
                    </div>

                    <button type="submit" name="submit" class="btn bg-transparent btn-outline-info text-info">Insert</button>
                </form>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>