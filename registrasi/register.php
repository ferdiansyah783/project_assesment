<?php

// koneksi
try{
    $conn= new PDO("mysql:host=localhost;dbname=mahasiswa","root","",[PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION]);
}catch (Exception $error){
    echo $error->getMessage();
}


function registrasi($data){
    global $conn;

    $username= strtolower(stripcslashes($data["username"]));
    $password= $data["password"];
    $password2= $data["password2"];

    // cek username
    $result=$conn->query("SELECT username FROM user WHERE username=$username");

    if ( $result->fetch(PDO::FETCH_ASSOC)){
        echo "<script>
        alert ('username sudah terdaftar');
        </script>";
    return false;
    }

    // cek konfirmasi password
    if ($password !== $password2){
        echo "<script>
            alert ('konfirmasi password tidak sesuai');
            </script>";
        return false;
    }
    // enskripsi password
    $password= password_hash($password, PASSWORD_DEFAULT);
    
    $result="INSERT INTO user(username,password) VALUES(?,?)";
    $reg=$conn->prepare($result);
    $reg->bindValue(1, $password,PDO::ATTR_DEFAULT_STR_PARAM);
    $reg->bindValue(2, $password2,PDO::ATTR_DEFAULT_STR_PARAM);

    $reg->execute();

    return $reg->rowCount();
}