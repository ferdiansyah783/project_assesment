<?php

// koneksi
try{
    $db= new PDO("mysql:host=localhost;dbname=project1","root","",[PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION]);
}catch (Exception $error){
    echo $error->getMessage();
}

// query
function comand($query){
    global $db;
    $result= $db->query($query);
    $rows=[];
    while($row=$result->fetch(PDO::FETCH_ASSOC)){
        $rows[]=$row;
    }
    return $rows;
}

// insert
function insert($data){
    global $db;
    // ambil data dari tiap elemen
    $nama=$data['nama'];
    $elemen=$data['elemen'];
    $skill=$data['skill'];
    
    // upload image
    $gambar=upload();
    if (!$gambar){
        return false;
    }

    // query
    $query="insert into pokemon(nama,elemen,skill,gambar) values (?,?,?,?)";
    $com=$db->prepare($query);
    $com->bindValue(1, $nama,PDO::ATTR_DEFAULT_STR_PARAM);
    $com->bindValue(2, $elemen,PDO::ATTR_DEFAULT_STR_PARAM);
    $com->bindValue(3, $skill,PDO::ATTR_DEFAULT_STR_PARAM);
    $com->bindValue(4, $gambar,PDO::ATTR_DEFAULT_STR_PARAM);

    $com->execute();

    return $com->rowCount();
}

// upload
function upload(){
    $nama_file=$_FILES['gambar']['name'];
    $ukuran_file=$_FILES['gambar']['size'];
    $error=$_FILES['gambar']['error'];
    $tmp_name=$_FILES['gambar']['tmp_name'];

    // cek apakah yg diupload adalah gambar
    $ekstensi_gambarValid=['jpg','jpeg','png'];
    $ekstensi_gambar=explode('.',$nama_file);
    $ekstensi_gambar=strtolower(end($ekstensi_gambar));
    if ( !in_array($ekstensi_gambar, $ekstensi_gambarValid)){
        echo "yang anda upload bukan gambar ";
        return false;
    }

    // cek jika ukuranya terlalu besar
    if ($ukuran_file > 700000){
        echo "ukuran gambar terlalu besar";
        return false;
    }

    // lolos pengecekan gambar
    // generate nama gambar baru
    $nama_fileBaru=uniqid();
    $nama_fileBaru .= '.';
    $nama_fileBaru .=$ekstensi_gambar;
    // var_dump($nama_fileBaru);die;

    move_uploaded_file($tmp_name, 'images/'. $nama_file);
    return $nama_file;

}

// delete
function delete($id){
    global $db;

    $com=$db->query("DELETE FROM pokemon WHERE id=$id");
    // $com->execute();

    return $com->rowCount();
}

// update
function update($data){
    global $db;
    // ambil data dari tiap elemen
    $id=$data['id'];
    $nama=$data['nama'];
    $elemen=$data['elemen'];
    $skill=$data['skill'];
    $gambarLama=$data['gambar'];
    // var_dump($gambarLama);die;

    // cek apakah user pilih gambar baru / tidak
    if ($_FILES['gambar']['error'] === 4){
        $gambar= $gambarLama;
    }else{
        $gambar=upload();
    }

    // query
    $query="UPDATE pokemon SET nama=?, elemen=?, skill=?, gambar=? WHERE id=?";
    $com=$db->prepare($query);
    $com->bindValue(1, $nama,PDO::ATTR_DEFAULT_STR_PARAM);
    $com->bindValue(2, $elemen,PDO::ATTR_DEFAULT_STR_PARAM);
    $com->bindValue(3, $skill,PDO::ATTR_DEFAULT_STR_PARAM);
    $com->bindValue(4, $gambar,PDO::ATTR_DEFAULT_STR_PARAM);
    $com->bindValue(5, $id,PDO::PARAM_INT);

    $com->execute();

    return $com->rowCount();
}

// filter
function searching($keyword){
    
    $query="SELECT * FROM pokemon WHERE nama LIKE '%$keyword%' OR elemen LIKE '%$keyword%' OR skill LIKE '%$keyword%' ";

    return comand($query);
    
}